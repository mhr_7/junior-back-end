import { IUserManager } from "interfaces/managers";
import { Component } from "merapi";
import { IUserRepo, Paginated } from "interfaces/repos";
import { IUser } from "interfaces/descriptors";

export default class UserManager extends Component implements IUserManager {
    constructor(private userRepo: IUserRepo) {
        super();
    }

    public listUser(
        query: Object,
        options?: {
            page?: number;
            limit?: number;
            sort?: { [column: string]: "asc" | "desc" };
        }
    ): Paginated<IUser> {}

    public descendingUsername(): Paginated<IUser> {
        return this.listUser({}, { sort: { username: "desc" } });
    }

    public readUser(username: string): IUser {}

    public deleteUser(username: string): boolean {}

    public register(object: IUser): IUser {
        const { username, email, password } = object;

        if (!username || !email || !password) {
            throw new Error("Username, email, and password are required.");
        }

        const user = this.userRepo.create(object);
        if (!user) {
            throw new Error(`User with username ${username} already exists.`);
        }
        return user;
    }

    public login(username: string, password: string): boolean {}

    public updateUser(username: string, object: IUser): IUser {}

    public updatePassword(
        username: string,
        oldPassword: string,
        newPassword: string
    ): IUser {}
}
