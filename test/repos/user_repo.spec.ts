import { suite, test, only, retries } from "mocha-typescript";
import { IUserRepo } from "interfaces/repos";
import UserRepo from "../../components/repos/user_repo";
import { IUser } from "interfaces/descriptors";
import { deepStrictEqual, strictEqual } from "assert";

@suite
class UserRepoTest {
    private userRepo: IUserRepo;
    private user1: IUser = {
        username: "1",
        name: "1",
        email: "1@gmail.com",
        password: "11111"
    };

    private user2: IUser = {
        username: "2",
        name: "2",
        email: "2@gmail.com",
        password: "11111"
    };

    private user3: IUser = {
        username: "3",
        name: "3",
        email: "3@gmail.com",
        password: "11111"
    };

    private user4: IUser = {
        username: "4",
        name: "4",
        email: "4@gmail.com",
        password: "11111"
    };

    private user5: IUser = {
        username: "5",
        name: "5",
        email: "5@gmail.com",
        password: "11111"
    };

    constructor() {
        this.userRepo = new UserRepo();
    }

    after() {
        this.userRepo.clear();
    }

    @test
    "should return user when successfully creating user"() {
        const user = this.userRepo.create(this.user1);
        deepStrictEqual(user, this.user1);
    }

    @test
    "should return null when creating user if another user with the same username exists"() {}

    @test
    "should return user when successfully reading user"() {
        const createdUser = this.userRepo.create(this.user1);
        const user = this.userRepo.read(this.user1.username);
        deepStrictEqual(user, createdUser);
    }

    @test
    "should return null when reading user if user not found"() {}

    @test
    "should be able to update user"() {}

    @test
    "should return null when updating user if user not found"() {}

    @test
    "should be able to delete user"() {}

    @test
    "should return false when deleting user if user not found"() {
        this.userRepo.create(this.user1);
        const deleted = this.userRepo.delete(this.user2.username);
        strictEqual(deleted, false);
    }

    @test
    "should be able to list user"() {
        this.userRepo.create(this.user1);
        this.userRepo.create(this.user2);
        this.userRepo.create(this.user3);
        this.userRepo.create(this.user4);
        this.userRepo.create(this.user5);

        const users = this.userRepo.list({});
        strictEqual(users.length, 5);
        deepStrictEqual(users, [
            this.user1,
            this.user2,
            this.user3,
            this.user4,
            this.user5
        ]);
    }

    @test
    "should be able to filter user when listing user"() {
        this.userRepo.create(this.user1);
        this.userRepo.create(this.user2);
        this.userRepo.create(this.user3);
        this.userRepo.create(this.user4);
        this.userRepo.create(this.user5);

        const users = this.userRepo.list({ username: "3" });
        strictEqual(users.length, 1);
        deepStrictEqual(users, [this.user3]);
    }

    @test
    "should be able to sort user when listing user"() {
        this.userRepo.create(this.user1);
        this.userRepo.create(this.user2);
        this.userRepo.create(this.user3);
        this.userRepo.create(this.user4);
        this.userRepo.create(this.user5);

        const users = this.userRepo.list({}, { sort: { username: "desc" } });
        strictEqual(users.length, 5);
        deepStrictEqual(users, [
            this.user5,
            this.user4,
            this.user3,
            this.user2,
            this.user1
        ]);
    }

    @test
    "should be able to paginate user when listing user"() {
        this.userRepo.create(this.user1);
        this.userRepo.create(this.user2);
        this.userRepo.create(this.user3);
        this.userRepo.create(this.user4);
        this.userRepo.create(this.user5);

        const users = this.userRepo.list({}, { page: 2, limit: 2 });
        strictEqual(users.length, 2);
        deepStrictEqual(users, [this.user3, this.user4]);
    }

    @test
    "should be able to count user"() {
        this.userRepo.create(this.user1);
        this.userRepo.create(this.user2);
        this.userRepo.create(this.user3);
        this.userRepo.create(this.user4);
        this.userRepo.create(this.user5);

        strictEqual(this.userRepo.count({}), 5);
    }

    @test
    "should return 0 when counting user if no user met criteria"() {
        this.userRepo.create(this.user1);
        this.userRepo.create(this.user2);
        this.userRepo.create(this.user3);
        this.userRepo.create(this.user4);
        this.userRepo.create(this.user5);

        strictEqual(this.userRepo.count({ username: "99" }), 0);
    }
}
